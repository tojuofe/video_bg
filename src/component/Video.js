import React, { Fragment } from 'react';
import vid_bg from './layout/Racing_Bike.mp4';
import '../App.css';

const Video = () => {
  return (
    <Fragment>
      <video className="responsive-video" autoPlay loop muted>
        <source src={vid_bg} type="video/mp4" />
      </video>
      <div className="overlayText card-panel reduceOpa hide-on-med-and-down">
        <h4 className="topText teal-text">Welcome to Train Time</h4>
        <form>
          <div className="input-field">
            <input type="text" />
            <label htmlFor="name">Name</label>
          </div>
          <div className="input-field">
            <input type="email" />
            <label htmlFor="email">Email</label>
          </div>
          <div className="input-field">
            <input type="password" />
            <label htmlFor="password">Password</label>
          </div>
          <input type="submit" className="btn btnWidth" />
        </form>
      </div>
    </Fragment>
  );
};

export default Video;
